require('async-to-gen/register')
var fs = require('fs')
var FetchImgs = require('./fetch-imgs')
var path = require('path')
const util = require('util')

var argv = process.argv
//入口链接
var url = argv[2]
//入口页面的超链选择器
var linkSelector = argv[3]
//入口页面的dom元素属性标签
var linkAttr = argv[4] || 'href';
//目标页面的图片选择器
var imgSelector = argv[5]
//目标页面的图片dom元素属性标签
var imgAttr = argv[6]
//图片分类目录
var folderName = argv[7] || Math.random().toString(36)
var directory = `./imgs/${folderName}`
fs.mkdirSync(directory)

// var mix = "http://jandan.net/ooxx/page-[1,2048]#comments"
// var mix = "http://www.16999.com/catalog-2-[1,306].html"
// var patt = new RegExp(/(\w+)(\d+)-(\d+)/,"g");
var patt = new RegExp(/(\w+)(:)(\/\/)([^\:|\/]+)(\:\d*)?(.*\/)([^#|\?|\n|\[]+)(\[\d+,\d+\])?([#.*]\w+)?(\?.*)?/, "g");
util.log(url)
var result = []
if ((result = patt.exec(url)) != null) {
    for (var i = 0; i < result.length; i++) {
        if (result[i] === undefined) {
            result[i] = ""
        }
        util.log("parse url:result[%d]=%s", i, result[i])
    }
}
var num = JSON.parse(result[8])
util.log("startNum:%d-endNum:%d", num[0], num[1])

for (let s = num[0]; s <= num[1]; s++) {
    var parseUrl = util.format("%s%d%s", result.slice(1, 8).join(""), s, result.slice(9, result.length).join(""))
    util.log("category enter:%s", parseUrl)
    FetchImgs.fetchLinks(parseUrl, linkSelector, linkAttr).then(function (links) {
        links.forEach(function (e, i) {
            //处理相对路径到绝对路径
            if (!e.startsWith("http://")) {
                var albumDir = Math.random().toString(36)
                var saveTo = path.join(directory, String(albumDir))
                fs.mkdirSync(saveTo)
                try {
                    for (let i = 1; i <= 10; i++) {
                        var destLink = util.format("%s%s-%d%s", result.slice(1, 6).join(""), e.substr(0, e.length - ".html".length), i, ".html")
                        util.log("destLink:%s", destLink)
                        FetchImgs.fetchImgs(destLink, imgSelector, imgAttr, saveTo)
                    }
                } catch (error) {
                    throw error
                }
            }
        })
    }).catch(error => util.log(error))
}


//运行方法
//node  common http://www.16999.com/catalog-2-[1,306].html '.dl-name>a' href '.big-pic img' src