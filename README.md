# spiderPic

## 图片抓取并上传到又拍云

## 运行环境

* node v7.7.1
* sqlite3

## 依赖包通过npm安装

* npm install


* 抓取[煎蛋妹子图](http://jandan.net/ooxx)2300页-2383页的图片选择器为 ```.text p img```

```
node  jandan http://jandan.net/ooxx/page-[2300,2383]#comments '.text p img'
```

* 抓取[东方网妹子图](http://mm.eastday.com)频道CSS选择器```#st_3```页面CSS选择器```data-url```图片CSS选择器```figure>a>img```

```
node mm-eastday http://mm.eastday.com 'figure>a>img' zhifu 'data-url' '#st_3
``` 
* 上传指定目录内的图片到又拍云

```
node upyun imgs/zhihu
```